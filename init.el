;; -*- coding: utf-8 -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Filename:     init.el
;;
;; Copyright (C) 2012-2014,  Luo Zikuan
;; Version:
;; Author:        Luo Zikuan <luozikuan@gmail.com>
;;
;; Created at:    Wed Aug 22 21:33:40 2012
;; Modified at:   Sun Dec  7 09:02:50 2014
;; Modified by:                luozikuan <luozikuan@Mint-PC>
;;
;; Description:   Luo Zikuan's customised Emacs
;;                This file must be ~/.emacs.d/init.el
;;               (For GNU Emacs24 only)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq emacs-load-start-time (current-time))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/initial"))

;;----------------------------------------------------------------------------
;; Which functionality to enable (use t or nil for true and false)
;;----------------------------------------------------------------------------
(setq *macbook-pro-support-enabled* t)
(setq *is-a-mac* (eq system-type 'darwin))
(setq *is-carbon-emacs* (and *is-a-mac* (eq window-system 'mac)))
(setq *is-cocoa-emacs* (and *is-a-mac* (eq window-system 'ns)))
(setq *win32* (eq system-type 'windows-nt) )
(setq *cygwin* (eq system-type 'cygwin) )
(setq *linux* (or (eq system-type 'gnu/linux) (eq system-type 'linux)) )
(setq *unix* (or *linux* (eq system-type 'usg-unix-v) (eq system-type 'berkeley-unix)) )
(setq *linux-x* (and window-system *linux*) )
(setq *xemacs* (featurep 'xemacs) )
(setq *emacs23* (and (not *xemacs*) (or (>= emacs-major-version 23))) )
(setq *emacs24* (and (not *xemacs*) (or (>= emacs-major-version 24))) )
(setq *no-memory* (cond
                   (*is-a-mac*
                    (< (string-to-number (nth 1 (split-string (shell-command-to-string "sysctl hw.physmem")))) 4000000000))
                   (*linux* nil)
                   (t nil)
                   ))

;----------------------------------------------------------------------------
; Functions (load all files in defuns-dir)
; Copied from https://github.com/magnars/.emacs.d/blob/master/init.el
;----------------------------------------------------------------------------
(setq defuns-dir (expand-file-name "~/.emacs.d/defuns"))
(dolist (file (directory-files defuns-dir t "\\w+"))
  (when (file-regular-p file)
      (load file)))

;----------------------------------------------------------------------------
; Load configs for specific features and modes
;----------------------------------------------------------------------------
(require 'init-modeline)

;;----------------------------------------------------------------------------
;; Load configs for specific features and modes
;;----------------------------------------------------------------------------
(require 'cl-lib)
(require 'init-compat)
(require 'init-utils)
(require 'init-site-lisp) ;; Must come before elpa, as it may provide package.el

;; win32 auto configuration, assuming that cygwin is installed at "c:/cygwin"
(condition-case nil
    (when *win32*
      (setq cygwin-mount-cygwin-bin-directory "c:/cygwin/bin")
      (require 'setup-cygwin)
      ;; better to set HOME env in GUI
      ;; (setenv "HOME" "c:/cygwin/home/someuser")
      )
  (error
   (message "setup-cygwin failed, continue anyway")
   ))

(require 'init-elpa)
(require 'init-exec-path) ;; Set up $PATH
(require 'init-frame-hooks)
;; any file use flyspell should be initialized after init-spelling.el
;; actually, I don't know which major-mode use flyspell.
;; (require 'init-spelling)
(require 'init-xterm)
;; (require 'init-osx-keys)
(require 'init-gui-frames)
(require 'init-maxframe)
;; (require 'init-proxies)
(require 'init-dired)
(require 'init-isearch)
(require 'init-uniquify)
(require 'init-ibuffer)
(require 'init-flymake)
(require 'init-artbollocks-mode)
(require 'init-recentf) ;; I don't know it's usage...
(require 'init-ido)
(require 'init-smex)
(if *emacs24* (require 'init-helm))
(require 'init-hippie-expand)
(require 'init-windows)
(require 'init-sessions)
(require 'init-fonts)
(require 'init-git)
;; (require 'init-crontab)
;; (require 'init-textile)
;; (require 'init-markdown)
;; (require 'init-csv)
;; (require 'init-erlang)
;; (require 'init-javascript)
;; (when *emacs24*
;;   (require 'init-org)
;;   (require 'init-org-mime))
;; (require 'init-css)
;; (require 'init-haml)
(require 'init-python-mode)
;; (require 'init-haskell)
;; (require 'init-ruby-mode)
(require 'init-lisp)
(require 'init-elisp)
(require 'init-yasnippet)
(require 'init-auto-header)
;; Use bookmark instead
;; (require 'init-zencoding-mode)
;; (require 'init-yari)
(require 'init-cc-mode)
(require 'init-gud)
(require 'init-semantic)
(require 'init-cmake-mode)
;; (require 'init-csharp-mode)
(require 'init-linum-mode)
;; (require 'init-emacs-w3m)
(require 'init-eim)
(require 'init-which-func)
;; (require 'init-keyfreq)
;; (require 'init-gist)
;; (require 'init-emacspeak)
(require 'init-pomodoro)
;; (require 'init-moz)
(require 'init-gtags)
;; use evil mode (vi key binding)
(require 'init-evil)
(require 'init-misc)
(require 'init-sh)
(require 'init-ctags)
(require 'init-ace-jump-mode)
;; (require 'init-bbdb)
;; (require 'init-gnus)
;; (require 'init-lua-mode)
;; (require 'init-doxygen)
;; (require 'init-workgroups2)
(require 'init-move-window-buffer)
(require 'init-term-mode)
;; (require 'init-web-mode)
(require 'init-sr-speedbar)
;; (require 'init-slime)
(when *emacs24* (require 'init-company))
(require 'init-stripe-buffer)
;; (require 'init-elnode)

;;----------------------------------------------------------------------------
;; Variables configured via the interactive 'customize' interface
;;----------------------------------------------------------------------------
(if (file-readable-p (expand-file-name "~/.custom.el"))
     (load-file (expand-file-name "~/.custom.el")))

;;----------------------------------------------------------------------------
;; Allow users to provide an optional "init-local" containing personal settings
;;----------------------------------------------------------------------------
(require 'init-local nil t)


;;----------------------------------------------------------------------------
;; Locales (setting them earlier in this file doesn't work in X)
;;----------------------------------------------------------------------------
;(require 'init-locales) ;does not work in cygwin


(when (require 'time-date nil t)
   (message "Emacs startup time: %d seconds."
    (time-to-seconds (time-since emacs-load-start-time)))
   )

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bmkp-last-as-first-bookmark-file "~/.emacs.bmk")
 '(safe-local-variable-values (quote ((emacs-lisp-docstring-fill-column . 75) (ruby-compilation-executable . "ruby") (ruby-compilation-executable . "ruby1.8") (ruby-compilation-executable . "ruby1.9") (ruby-compilation-executable . "rbx") (ruby-compilation-executable . "jruby")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(window-numbering-face ((t (:foreground "DeepPink" :underline "DeepPink" :weight bold))) t))
;;; Local Variables:
;;; no-byte-compile: t
;;; End:
(put 'erase-buffer 'disabled nil)

;;----------------------------------------------------------------------------
;; settings written by luozikuan
;;----------------------------------------------------------------------------

;; some key binding
;; F2 to actived mark
(global-set-key [f2] 'set-mark-command)
;; F5 uncomment region
;; C-F5 comment region
(global-set-key [f5] 'uncomment-region)
(global-set-key [(control f5)] 'comment-region)
;; F8 reload file with given encoding
(global-set-key [f8] 'revert-buffer-with-coding-system)
;; F11 align
(global-set-key [f11] 'align-regexp)
;; M-g goto line
(global-set-key [(meta g)] 'goto-line)
;; press ESC twice to enable vi-mode
(global-set-key (kbd "\e\e") 'evil-mode)
;; C-. mark the postion, C-, jump between current and marked position
(global-set-key [(control ?\.)] 'ska-point-to-register)
(global-set-key [(control ?\,)] 'ska-jump-to-register)
(defun ska-point-to-register()
  "Store cursorposition _fast_ in a register.
Use ska-jump-to-register to jump back to the stored
position."
  (interactive)
  (setq zmacs-region-stays t)
  (point-to-register 8))
(defun ska-jump-to-register()
  "Switches between current cursorposition and position
that was stored with ska-point-to-register."
  (interactive)
  (setq zmacs-region-stays t)
  (let ((tmp (point-marker)))
    (jump-to-register 8)
    (set-register 8 tmp)))

;; make ASCII comment Italic
(make-face-italic 'font-lock-comment-face)
;; set tab width to spaces
(setq-default indent-tabs-mode nil)
;; mouse dodge cursor
(mouse-avoidance-mode 'animate)
;; scroll at 1 near start or end
(setq scroll-margin 1
      scroll-conservatively 10000)
;; set kill ring size to 150
(setq kill-ring-max 150)
;; set my identity and mail
(setq user-full-name "Luo ZiKuan")
(setq user-mail-address "luozikuan@gmail.com")

;; ;; auctex for edit latex
;; (load "auctex.el" nil t t)
;; (load "preview-latex.el" nil t t)

;;----------------------------------------------------------------------------
;; luozikuan's own settings ends here
;;----------------------------------------------------------------------------
